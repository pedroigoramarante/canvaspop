import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Flavour } from 'src/app/models/flavour';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Order } from 'src/app/models/order';

@Component({
  selector: 'app-form-order',
  templateUrl: './form-order.component.html',
  styleUrls: ['./form-order.component.scss']
})
export class FormOrderComponent implements OnInit {

  flavours: Flavour[] = [];
  selected: number;
  currentFlavour: Flavour;
  form: FormGroup;
  submitted: boolean = false;
  success: boolean = false;

  constructor(private apiService: ApiService, private formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      orderedBy: ['', Validators.required],
      flavour: ['', Validators.required],
      shouldSubscribe: [false, Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadFlavours();
  }

  loadFlavours(): void {
    this.apiService.getFlavors().subscribe((res) => {
      this.flavours = res;

      if (this.flavours[0]) {
        this.selectFlavour(this.flavours[0]);
      }
    }, (err) => {
      console.log(err);
    });
  }

  selectFlavour(flavour: Flavour): void {
    this.currentFlavour = flavour;
    this.selected = flavour.id;
    this.form.controls.flavour.setValue(flavour.id);
  }

  onSubmit(): void {
    this.success = false;
    this.submitted = true;
    this.form.disable();

    const data: Order = this.form.value;

    setTimeout(() => {
      this.apiService.newOrder(data).subscribe((res) => {
        this.clearForm();
        this.submitted = false;
        this.form.enable();
        this.success = true;
      }, (err) => {
        console.log(err);
        this.submitted = false;
        this.form.enable();
      });
    }, 3000);
  }

  clearForm(): void {
    this.form.controls.orderedBy.setValue('');

    if (this.flavours[0]) {
      this.selectFlavour(this.flavours[0]);
    }

    this.form.controls.shouldSubscribe.setValue(false);
  }

  back(): void {
    this.success = false;
  }

}
