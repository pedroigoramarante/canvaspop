import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Flavour } from '../models/flavour';
import { Order } from '../models/order';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  env: any = environment;

  constructor(private httpClient: HttpClient) { }

  getFlavors(): Observable<Flavour[]> {
    return this.httpClient.get<Flavour[]>(this.env.apiUrl + 'flavours');
  }

  newOrder(order: Order): Observable<Order> {
    return this.httpClient.post<Order>(this.env.apiUrl + 'orders', order);
  }
}
