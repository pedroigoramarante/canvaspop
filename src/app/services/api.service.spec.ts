import { HttpClientModule } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { ApiService } from "./api.service";
import { Flavour } from '../models/flavour';

describe('Api Service', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [ApiService]
        });
    });

    it('Flavors should return more than 0', inject([ApiService], (service: ApiService) => {
        service.getFlavors().subscribe((res: Flavour[]) => {
            expect(res.length).toBeGreaterThan(0);
        });
    }));
})