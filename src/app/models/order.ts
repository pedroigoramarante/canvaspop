export interface Order {
    id?: number,
    orderedBy: string,
    flavour: number,
    shouldSubscribe: boolean
}
