export interface Flavour {
    id?: number,
    title: string,
    image: string
}
